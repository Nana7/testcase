import React, { Component } from 'react'
import {isMobile} from 'react-device-detect'

import CForm from './components/js/cForm'
import CListReview from './components/js/cListReview'
import ServiceReview from './services/serviceReview'


class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            data : [],
            edit : []
        }

        this.serviceReview = new ServiceReview()
        this.edit = this.edit.bind(this)
    }

    componentDidMount(){
        this.serviceReview.getReviewAll().then(res=> { this.setState({data: res.data})})
    }

    edit(e, key){
        e.preventDefault();
        this.serviceReview.getReview(key).then(res=> { this.setState({edit: res.data})})
        // await console.log("triggered")
    }

    render() {
        if(isMobile){
            return(
                <div className="container-fluid pt-3">
                    <CForm edit={this.state.edit}/>
                    <div className="dropdown-divider"></div>
                    <CListReview review={this.state.data} editClick={this.edit}/>
                </div>
            )
        } 
        else{
            return null
        }  
    }
}

export default App;
