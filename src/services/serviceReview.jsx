import axios from 'axios'

class ServiceReview{
    getReview(key){
        return axios.get('http://localhost:8000/reviews/'+key).then(res=>{return res})
    }

    getReviewAll(){
        return axios.get('http://localhost:8000/reviews/getall').then(res=>{return res})
    }

    postReview(body){
        return axios.post('http://localhost:8000/reviews/create', body , {headers:{'Content-Type' : 'application/json'}})
    }

    putReview(params, body){
        return axios.put('http://localhost:8000/reviews/'+params+'/update', body , {headers:{'Content-Type' : 'application/json'}})
    }

    deleteReview(params){
        return axios.delete('http://localhost:8000/reviews/'+params+'/delete',).then(res=>{return res.status})
    }

}

export default ServiceReview