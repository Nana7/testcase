import React,{Component} from 'react'
import CRating from './cRating'
import ServiceReview from '../../services/serviceReview'

class CForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            data : []
        }

        this.formData = {
            _id : '',
            jmlBintang : 0,
            gambar : '',
            textReview : '',
            user : 'lorem',
            waktu : new Date
        }

        this.serviceReview = new ServiceReview()
    }

    componentDidUpdate(pervProps){
        if(this.props.edit._id !== pervProps.edit._id){
            this.formData._id = this.props.edit._id
            this.formData.jmlBintang = this.props.edit.jmlBintang
            this.formData.gambar = this.props.edit.gambar
            this.formData.textReview = this.props.edit.textReview
        }
        // console.log(this.props.edit)
    }

    handleSubmit(e){
        e.preventDefault()
        if(this.formData._id===''){
            this.serviceReview.postReview(JSON.stringify(this.formData))
        }
        else{
            this.serviceReview.putReview(this.formData._id, JSON.stringify(this.formData))
        }
        // window.location = "/"
    }

    handleChange(field, e){
        let FR = new FileReader()
        if(field==="inputGambar"){
            let imageFile = document.querySelector('#inputGambar').files[0]
            FR.readAsDataURL(imageFile);
            FR.onload = (e) => { this.formData['gambar'] = FR.result }
        }else{
            this.formData[field] = e.target.value
        }

    }

    render(){
        return(
            <form encType="multipart/form-data" onSubmit={(e)=>{this.handleSubmit(e)}}>
                <input type="hidden" value={this.formData._id} onChange={(e)=>{this.handleChange('_id', e)}}  />
                <div className="clearfix">
                    <div className="float-left"><h3>Review</h3></div>
                    <div className="float-right"><CRating edit={this.formData.jmlBintang} change={(e)=>{this.handleChange('jmlBintang', e)}} /></div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="form-row">
                            <div className="form-group col-12">
                                <textarea className="form-control" rows="5" placeholder="Tulis Ulasan Terbaikmu" value={this.formData.textReview} onChange={(e)=>{this.handleChange('textReview', e)}}></textarea>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group col-6">
                                <input type="file" name="" id="inputGambar" placeholder="Upload gambar" onChange={(e)=>{this.handleChange('inputGambar', e)}}/>
                            </div>
                            <div className="form-group col-6">
                                <button type="submit" className="btn btn-primary rounded-0 float-right">Kirim</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}

export default CForm