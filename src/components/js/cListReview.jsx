import React,{Component} from 'react'
import ServiceReview from '../../services/serviceReview'

class CListReview extends Component{
    constructor(props){
        super(props)
        this.state = {
            data : []
        }

        this.serviceReview = new ServiceReview()

        this.handleDelete = this.handleDelete.bind(this)
    }

    static getDerivedStateFromProps(nextProps){
        return{
            data :nextProps.review
        }
    }
    

    handleDelete(e, params){
        e.preventDefault()
        this.serviceReview.deleteReview(params)
        window.location = "/"
    }

    star(count){
        let star = []
        for(var i=0; i<count;i++){
            star.push(<i className="fa fa-star text-warning" key={i}></i>)
        }
        return star
    }
    

    render(){
        if(this.state.data.length !== 0 ){
            return this.state.data.map((data, i)=>{
                return(
                    <div className="row mb-3" key={i}>
                        <div className="col-12 mb-2">
                            <div className="media">
                                <img src="https://via.placeholder.com/75x75.png" className="mr-3 rounded-circle" alt="..." />
                                <div className="media-body">
                                    <div className="clearfix">
                                        <div className="float-left"><h6 className="mb-0">Afdha</h6></div>
                                        <div className="float-right">
                                            <span><a href="" onClick={(e)=>{this.handleDelete(e, data._id)}}>Hapus </a></span>|
                                            <span><a href="" onClick={(e)=>this.props.editClick(e, data._id)}> Edit</a></span>
                                        </div>
                                    </div>
                                    <span>{data.textReview}</span><br/>
                                    <span className="text-muted">{data.waktu}</span><br/>
                                    <span> 
                                        { this.star(data.jmlBintang) }
                                    </span>
                                    <p>{data.review}</p>
    
                                    <div className="row">
                                        <div className="col-6">
                                            <div className="card">
                                                <img src={data.gambar} className="card-img-top"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                )
            })
        }
        else{
            return null
        }
    }
}

export default CListReview